const app = require('./src/app');

const PORT = 8080;

app.listen(PORT, () => console.log(`Running a GraphQL server at http://localhost:${PORT}/graphql`));
