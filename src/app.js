const express = require('express');
const expressGraphQL = require('express-graphql');
const mongoose = require('mongoose');
const session = require('express-session');
const passport = require('passport');
const connectMongo = require('connect-mongo');
const cors = require('cors');
const keys = require('./config/keys');
require('./models/UserModel');
require('./services/passport');

const schema = require('./schema/schema');

const app = express();

// Mongoose's built in promise library is deprecated, replace it with ES2015 Promise
mongoose.Promise = global.Promise;
// Connect to mongoDb instance
mongoose.connect(keys.mongoURI);
mongoose.connection
  .once('open', () => console.log('Connected to MongoLab DB instance'))
  .on('error', error => console.error('Error connecting to MongoLab: ', error));

const corsMiddleware = cors({
  origin: keys.clientURL, // /nutrifymeapp\.com$/,
  credentials: true,
  preflightContinue: false,
});
app.use(corsMiddleware);
app.options(corsMiddleware);

// Configures express to use sessions.  This places an encrypted identifier
// on the users cookie.  When a user makes a request, this middleware examines
// the cookie and modifies the request object to indicate which user made the request
// The cookie itself only contains the id of a session; more data about the session
// is stored inside of MongoDB.
const MongoStore = connectMongo(session);
app.use(
  session({
    resave: true,
    saveUninitialized: true,
    secret: keys.sessionSecret,
    store: new MongoStore({
      url: keys.mongoURI,
      autoReconnect: true,
    }),
  })
);

// Passport is wired into express as a middleware. When a request comes in,
// Passport will examine the request's session (as set by the above config) and
// assign the current user to the 'req.user' object.  See also servces/auth.js
app.use(passport.initialize());
app.use(passport.session());

// routes to handle auth by third party providers using passport strategies
require('./routes/providersRoutes')(app);

// Instruct Express to pass on any request made to the '/graphql' route
// to the GraphQL instance.
app.use(
  '/',
  expressGraphQL(req => {
    return {
      schema,
      graphiql: true,
      context: {
        user: req.user,
        req,
      },
    };
  })
);

module.exports = app;
