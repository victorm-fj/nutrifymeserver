const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const {
  squaredHeight,
  calcBMI,
  getBmiIntervalIndex,
  getInterval,
  getLegend,
  calcIdealBW,
  calcMinBW,
  calcMaxBW,
} = require('../utils/resultsLogic');
const {
  calcTEE,
  calcCarbs,
  calcProts,
  calcFats,
} = require('../utils/needsLogic');

const ProfileSchema = new Schema({
  date: String,
  userId: String,
  age: Number,
  weight: Number,
  height: Number,
  gender: String,
  activityLevel: Number,
  goal: { type: Number, default: 0 },
  calRestriction: { type: Number, default: 0 },
  results: {
    bmi: Number,
    interval: String,
    legend: String,
    idealBW: Number,
    minBW: Number,
    maxBW: Number,
  },
  needs: {
    tee: Number,
    carbs: Number,
    prots: Number,
    fats: Number,
  },
});

function preCallback(next) {
  const { age, weight, height, gender, activityLevel, calRestriction } = this;
  const height2 = squaredHeight(height);

  const bmi = calcBMI(weight, height2);
  const bmiIntervalIndex = getBmiIntervalIndex(bmi);
  const interval = getInterval(bmiIntervalIndex);
  const legend = getLegend(bmiIntervalIndex);
  const idealBW = calcIdealBW(height2);
  const minBW = calcMinBW(height2);
  const maxBW = calcMaxBW(height2);

  const tee =
    calcTEE(age, weight, height, gender, activityLevel) - calRestriction;
  const carbs = calcCarbs(tee);
  const prots = calcProts(tee);
  const fats = calcFats(tee);

  this.results = {
    bmi,
    interval,
    legend,
    idealBW,
    minBW,
    maxBW,
  };

  this.needs = {
    tee,
    carbs,
    prots,
    fats,
  };

  next();
}

ProfileSchema.pre('save', preCallback);

const Profile = mongoose.model('profile', ProfileSchema);

module.exports = Profile;
