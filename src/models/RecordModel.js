const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const RecordSchema = new Schema({
  date: String,
  profileId: String,
  foods: [
    {
      meal: String,
      food: { type: Schema.Types.ObjectId, ref: 'food' },
      quantity: String,
      units: String,
    },
  ],
});

const Record = mongoose.model('record', RecordSchema);

module.exports = Record;
