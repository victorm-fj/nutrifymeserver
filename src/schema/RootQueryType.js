const graphql = require('graphql');

const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLNonNull,
  GraphQLList,
} = graphql;

const UserType = require('./UserType');
const ProfileType = require('./ProfileType');
const RecordType = require('./RecordType');
const FoodType = require('./FoodType');
const { getProfile, getProfiles } = require('../services/profile');
const { getRecord } = require('../services/record');
const { getFood } = require('../services/food');

const RootQueryType = new GraphQLObjectType({
  name: 'RootQueryType',
  fields: {
    user: {
      type: UserType,
      resolve(parentValue, args, req) {
        // if loged in returns user, if not returns undefined which graphql turns into null
        return req.user;
      },
    },

    profile: {
      type: ProfileType,
      args: {
        date: { type: new GraphQLNonNull(GraphQLString) },
      },
      resolve(parentValue, { date }, req) {
        return getProfile(req, date);
      },
    },

    record: {
      type: RecordType,
      args: {
        date: { type: GraphQLString },
        profileId: { type: GraphQLString },
      },
      resolve(parentValue, { date, profileId }, req) {
        return getRecord(req, date, profileId);
      },
    },

    foods: {
      type: new GraphQLList(FoodType),
      args: {
        description: { type: GraphQLString },
      },
      resolve(parentValue, { description }, req) {
        return getFood(req, description);
      },
    },

    profiles: {
      type: new GraphQLList(ProfileType),
      resolve(parentValue, args, req) {
        return getProfiles(req);
      },
    },
  },
});

module.exports = RootQueryType;
