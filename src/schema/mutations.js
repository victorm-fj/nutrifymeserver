const graphql = require('graphql');

const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLInt,
  GraphQLFloat,
  GraphQLID,
  GraphQLNonNull,
} = graphql;

const UserType = require('./UserType');
const ProfileType = require('./ProfileType');
const RecordType = require('./RecordType');
const { signup, login } = require('../services/auth');
const { createProfile, updateProfile } = require('../services/profile');
const { updateRecord, updateFood, deleteFood } = require('../services/record');
const { deleteAccount } = require('../services/account');

const mutation = new GraphQLObjectType({
  name: 'Mutation',
  fields: {
    signup: {
      type: UserType,
      args: {
        email: { type: GraphQLString },
        password: { type: GraphQLString },
      },
      resolve(parentValue, { email, password }, { req }) {
        return signup({ email, password, req });
      },
    },

    logout: {
      type: UserType,
      resolve(parentValue, args, context) {
        const { user, req } = context;
        req.logout();
        return user;
      },
    },

    login: {
      type: UserType,
      args: {
        email: { type: GraphQLString },
        password: { type: GraphQLString },
      },
      resolve(parentValue, { email, password }, { req }) {
        return login({ email, password, req });
      },
    },

    createProfile: {
      type: ProfileType,
      args: {
        date: { type: GraphQLString },
        unitSystem: { type: GraphQLInt },
        age: { type: GraphQLInt },
        weight: { type: GraphQLFloat },
        height: { type: GraphQLFloat },
        gender: { type: GraphQLString },
        activityLevel: { type: GraphQLInt },
        goal: { type: GraphQLInt },
        calRestriction: { type: GraphQLInt },
      },
      resolve(parentValue, args, { req }) {
        return createProfile(args, req);
      },
    },

    updateProfile: {
      type: ProfileType,
      args: {
        id: { type: new GraphQLNonNull(GraphQLID) },
        date: { type: new GraphQLNonNull(GraphQLString) },
        unitSystem: { type: GraphQLInt },
        age: { type: GraphQLInt },
        weight: { type: GraphQLFloat },
        height: { type: GraphQLFloat },
        gender: { type: GraphQLString },
        activityLevel: { type: GraphQLInt },
        goal: { type: GraphQLInt },
        calRestriction: { type: GraphQLInt },
      },
      resolve(parentValue, args, { req }) {
        return updateProfile(args, req);
      },
    },

    updateRecord: {
      type: RecordType,
      args: {
        recordId: { type: GraphQLString },
        meal: { type: GraphQLString },
        foodId: { type: GraphQLString },
        quantity: { type: GraphQLString },
        units: { type: GraphQLString },
      },
      resolve(parentValue, args, { req }) {
        return updateRecord(args, req);
      },
    },

    updateFood: {
      type: RecordType,
      args: {
        id: { type: new GraphQLNonNull(GraphQLID) },
        quantity: { type: new GraphQLNonNull(GraphQLString) },
        recordId: { type: new GraphQLNonNull(GraphQLID) },
      },
      resolve(parentValue, args, { req }) {
        return updateFood(args, req);
      },
    },

    deleteFood: {
      type: RecordType,
      args: {
        id: { type: new GraphQLNonNull(GraphQLID) },
        recordId: { type: new GraphQLNonNull(GraphQLID) },
      },
      resolve(parentValue, args, { req }) {
        return deleteFood(args, req);
      },
    },

    deleteAccount: {
      type: UserType,
      resolve(parentValue, args, { req }) {
        return deleteAccount(req);
      },
    },
  },
});

module.exports = mutation;
