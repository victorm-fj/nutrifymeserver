const graphql = require('graphql');

const {
  GraphQLObjectType,
  GraphQLList,
  GraphQLString,
  GraphQLID,
  GraphQLFloat,
} = graphql;

const FoodItem = new GraphQLObjectType({
  name: 'FoodItem',
  fields: {
    id: { type: GraphQLID },
    description: { type: GraphQLString },
    kcal: { type: GraphQLFloat },
    protein_g: { type: GraphQLFloat },
    lipid_total_g: { type: GraphQLFloat },
    carbohydrate_g: { type: GraphQLFloat },
    fiber_td_g: { type: GraphQLFloat },
    fa_sat_g: { type: GraphQLFloat },
    fa_mono_g: { type: GraphQLFloat },
    fa_poly_g: { type: GraphQLFloat },
    gmwt_1: { type: GraphQLFloat },
    gmwt_desc1: { type: GraphQLString },
    gmwt_2: { type: GraphQLFloat },
    gmwt_desc2: { type: GraphQLString },
  },
});

const FoodsType = new GraphQLObjectType({
  name: 'FoodsType',
  fields: {
    id: { type: GraphQLID },
    meal: { type: GraphQLString },
    food: { type: FoodItem },
    quantity: { type: GraphQLString },
    units: { type: GraphQLString },
  },
});

const RecordType = new GraphQLObjectType({
  name: 'RecordType',
  fields: {
    id: { type: GraphQLID },
    date: { type: GraphQLString },
    profileId: { type: GraphQLString },
    foods: { type: new GraphQLList(FoodsType) },
  },
});

module.exports = RecordType;
