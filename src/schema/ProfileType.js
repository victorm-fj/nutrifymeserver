const graphql = require('graphql');

const {
  GraphQLObjectType,
  GraphQLString,
  GraphQLID,
  GraphQLInt,
  GraphQLFloat,
} = graphql;

const ResultsType = new GraphQLObjectType({
  name: 'ResultsType',
  fields: {
    bmi: { type: GraphQLFloat },
    interval: { type: GraphQLString },
    legend: { type: GraphQLString },
    idealBW: { type: GraphQLFloat },
    minBW: { type: GraphQLFloat },
    maxBW: { type: GraphQLFloat },
  },
});

const NeedsType = new GraphQLObjectType({
  name: 'NeedsType',
  fields: {
    tee: { type: GraphQLInt },
    carbs: { type: GraphQLInt },
    prots: { type: GraphQLInt },
    fats: { type: GraphQLInt },
  },
});

const ProfileType = new GraphQLObjectType({
  name: 'ProfileType',
  fields: {
    id: { type: GraphQLID },
    date: { type: GraphQLString },
    unitSystem: { type: GraphQLInt },
    age: { type: GraphQLInt },
    weight: { type: GraphQLFloat },
    height: { type: GraphQLFloat },
    gender: { type: GraphQLString },
    activityLevel: { type: GraphQLInt },
    goal: { type: GraphQLInt },
    calRestriction: { type: GraphQLInt },
    results: { type: ResultsType },
    needs: { type: NeedsType },
  },
});

module.exports = ProfileType;
