const GraphQLSchema = require('graphql').GraphQLSchema;

const RootQueryType = require('./RootQueryType');
const mutation = require('./mutations');

const schema = new GraphQLSchema({
  query: RootQueryType,
  mutation,
});

module.exports = schema;
