const graphql = require('graphql');

const { GraphQLObjectType, GraphQLString, GraphQLID } = graphql;

const UserType = new GraphQLObjectType({
  name: 'UserType',
  fields: {
    id: { type: GraphQLID },
    email: { type: GraphQLString },
    googleId: { type: GraphQLString },
    facebookId: { type: GraphQLString },
    twitterId: { type: GraphQLString },
  },
});

module.exports = UserType;
