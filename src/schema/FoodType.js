const graphql = require('graphql');

const { GraphQLObjectType, GraphQLString, GraphQLID, GraphQLFloat } = graphql;

const FoodType = new GraphQLObjectType({
  name: 'FoodType',
  fields: {
    id: { type: GraphQLID },
    description: { type: GraphQLString },
    kcal: { type: GraphQLFloat },
    protein_g: { type: GraphQLFloat },
    lipid_total_g: { type: GraphQLFloat },
    carbohydrate_g: { type: GraphQLFloat },
    fiber_td_g: { type: GraphQLFloat },
    fa_sat_g: { type: GraphQLFloat },
    fa_mono_g: { type: GraphQLFloat },
    fa_poly_g: { type: GraphQLFloat },
    gmwt_1: { type: GraphQLFloat },
    gmwt_desc1: { type: GraphQLString },
    gmwt_2: { type: GraphQLFloat },
    gmwt_desc2: { type: GraphQLString },
  },
});

module.exports = FoodType;
