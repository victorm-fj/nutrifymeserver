const Food = require('../models/FoodModel');

const getFood = (req, description) => {
  if (!req.user) {
    throw new Error('Must be a register user to perform this action.');
  }

  return Food.find({ description: { $regex: description, $options: 'i' } })
    .limit(20)
    .exec()
    .then(food => {
      return food;
    })
    .catch(err => {
      throw new Error(`Something went wrong. Error: ${err}`);
    });
};

module.exports = {
  getFood,
};
