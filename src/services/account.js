const User = require('../models/UserModel');

const deleteAccount = req => {
  if (!req.user) {
    throw new Error('Must be a register user to perform this action.');
  }

  return User.findById(req.user.id)
    .then(userDoc => {
      userDoc.remove();

      const { user } = req;
      req.logout();
      return user;
    })
    .catch(err => console.log(`Something went wrong. ${err}`));
};

module.exports = { deleteAccount };
