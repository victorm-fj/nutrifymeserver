const _ = require('lodash');
const Profile = require('../models/ProfileModel');
const { createRecord } = require('./record');

const createProfile = (profile, req) => {
  if (!req.user) {
    throw new Error('Must be a register user to perform this action.');
  }

  profile.userId = req.user._id.toString();
  const userProfile = new Profile(profile);

  return userProfile
    .save()
    .then(savedProfile => {
      const { date, _id } = savedProfile;
      createRecord(date, _id);
      return savedProfile;
    })
    .catch(err => {
      throw new Error(`Something went wrong. Error: ${err}`);
    });
};

const getProfile = (req, date) => {
  if (!req.user) {
    throw new Error('Must be a register user to perform this action.');
  }

  return Profile.find({ userId: req.user._id.toString() })
    .sort('date')
    .exec()
    .then(profiles => {
      const sameOrBeforeProfiles = _.filter(profiles, profile => {
        return profile.date <= date;
      });
      return _.last(sameOrBeforeProfiles);
    })
    .catch(err => {
      throw new Error(`Something went wrong. Error: ${err}`);
    });
};

const updateProfile = (profile, req) => {
  if (!req.user) {
    throw new Error('Must be a register user to perform this action.');
  }

  return Profile.findOne({ _id: profile.id, date: profile.date })
    .then(profileDoc => {
      // if profile exists
      if (profileDoc) {
        const updatedProfile = _.merge(profileDoc, profile);
        return updatedProfile.save().then(savedProfile => savedProfile);
      }

      // if profile doesn't exists
      return Profile.findOne({ _id: profile.id }, { _id: 0 }).then(doc => {
        const updatedProfile = _.merge(doc.toObject(), profile);
        const userProfile = new Profile(updatedProfile);

        return userProfile.save().then(savedProfile => {
          return savedProfile;
        });
      });
    })
    .catch(err => {
      throw new Error(`Something went wrong. Error: ${err}`);
    });
};

const getProfiles = req => {
  if (!req.user) {
    throw new Error('Must be a register user to perform this action.');
  }

  return Profile.find({ userId: req.user._id.toString() })
    .sort('date')
    .exec()
    .then(profiles => {
      return profiles;
    })
    .catch(err => {
      throw new Error(`Something went wrong. Error: ${err}`);
    });
};

module.exports = {
  createProfile,
  getProfile,
  updateProfile,
  getProfiles,
};
