const _ = require('lodash');
const Record = require('../models/RecordModel');

const createRecord = (date, profileId) => {
  const userRecord = new Record({ date, profileId });

  return userRecord.save().then(record => record).catch(err => {
    throw new Error(`Something went wrong. Error: ${err}`);
  });
};

const getRecord = (req, date, profileId) => {
  if (!req.user) {
    throw new Error('Must be a register user to perform this action.');
  }

  return Record.findOne({ date, profileId })
    .populate({
      path: 'foods.food',
      select:
        'description kcal protein_g lipid_total_g carbohydrate_g fiber_td_g fa_sat_g fa_mono_g fa_poly_g gmwt_1 gmwt_desc1 gmwt_2 gmwt_desc2',
    })
    .exec()
    .then(populatedRecord => {
      if (!populatedRecord) return createRecord(date, profileId);
      return populatedRecord;
    })
    .catch(err => {
      throw new Error(`Something went wrong. Error: ${err}`);
    });
};

const updateRecord = (args, req) => {
  if (!req.user) {
    throw new Error('Must be a register user to perform this action.');
  }

  const { recordId, meal, foodId, quantity, units } = args;

  return Record.findById(recordId)
    .then(record => {
      const foodItem = { meal, food: foodId, quantity, units };
      record.foods.push(foodItem);

      return record.save().then(updatedRecord => updatedRecord);
    })
    .catch(err => {
      throw new Error(`Something went wrong. Error: ${err}`);
    });
};

const updateFood = (args, req) => {
  if (!req.user) {
    throw new Error('Must be a register user to perform this action.');
  }

  const { id, quantity, recordId } = args;

  return Record.findById(recordId).then(record => {
    record.foods.forEach(food => {
      if (food._id.toString() === id) {
        _.merge(food, { quantity });
      }
    });

    return record.save().then(updatedRecord => updatedRecord);
  });
};

const deleteFood = (args, req) => {
  if (!req.user) {
    throw new Error('Must be a register user to perform this action.');
  }

  const { id, recordId } = args;

  return Record.findById(recordId).then(record => {
    record.foods.pull({ _id: id });
    return record.save().then(updatedRecord => updatedRecord);
  });
};

module.exports = {
  createRecord,
  getRecord,
  updateRecord,
  updateFood,
  deleteFood,
};
