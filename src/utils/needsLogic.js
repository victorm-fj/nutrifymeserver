// Calculates user's Total Energetic Expenditure 'TEE'
const calcTEE = (age, weight, height, gender, activityLevel) => {
  let af; // activity factor
  let bee; // basal energetic expenditure

  // Activity levels from https://www.nap.edu/read/1349/chapter/4#29
  switch (activityLevel) {
    case 1:
      af = 1.26; // 1.2 + 0.06 as thermic effect of food
      break;
    case 2:
      af = 1.435; // 1.375 + 0.06 as thermic effect of food
      break;
    case 3:
      af = 1.61; // 1.55 + 0.06 as thermic effect of food
      break;
    case 4:
      af = 1.785; // 1.725 + 0.06 as thermic effect of food
      break;
    case 5:
      af = 1.96; // 1.9 + 0.06 as thermic effect of food
      break;
    default:
      break;
  }

  // Ecuations from http://www-users.med.cornell.edu/~spon/picu/calc/beecalc.htm
  if (gender === 'female') {
    bee = 655.1 + (9.563 * weight + 1.85 * height - 4.676 * age);
  } else {
    bee = 66.47 + (13.75 * weight + 5.003 * height - 6.755 * age);
  }

  // total energy expenditure
  const tee = Math.round(bee * af);

  return tee;
};

// Calculates daily recommended carbohydrates in g
const calcCarbs = tee => {
  return Math.round(tee * 0.6 / 4);
};

// Calculates daily recommended fats in g
const calcFats = tee => {
  return Math.round(tee * 0.25 / 9);
};

// Calculates daily recommended proteins in g
const calcProts = tee => {
  return Math.round(tee * 0.15 / 4);
};

module.exports = {
  calcTEE,
  calcCarbs,
  calcProts,
  calcFats,
};
