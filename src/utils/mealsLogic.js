const assignMeals = foodsNumber => {
  switch (foodsNumber) {
    case 3: {
      const meals = ['Breakfast', 'Lunch', 'Dinner'];
      return meals.map(meal => ({ name: meal }));
    }

    case 4: {
      const meals = ['Breakfast', 'Lunch', 'Afternoon snack', 'Dinner'];
      return meals.map(meal => ({ name: meal }));
    }

    case 5: {
      const meals = [
        'Breakfast',
        'Mid-morning snack',
        'Lunch',
        'Afternoon snack',
        'Dinner',
      ];
      return meals.map(meal => ({ name: meal }));
    }

    default:
      return [];
  }
};

module.exports = {
  assignMeals,
};
