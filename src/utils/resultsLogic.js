const bmiIntervals = require('./bmiIntervals.json');

const squaredHeight = height => {
  return Math.pow(height / 100, 2);
};

const calcBMI = (weight, squaredHeight) => {
  return Math.round(weight / squaredHeight * 10) / 10;
};

const calcMinBW = squaredHeight => {
  return Math.round(squaredHeight * 18.5 * 10) / 10;
};

const calcMaxBW = squaredHeight => {
  return Math.round(squaredHeight * 24.99 * 10) / 10;
};

const calcIdealBW = squaredHeight => {
  return Math.round(squaredHeight * 22.1 * 10) / 10;
};

const getBmiIntervalIndex = bmi => {
  // body mass index
  if (bmi < 18.5) {
    return 0;
  } else if (bmi >= 18.5 && bmi <= 24.9) {
    return 1;
  } else if (bmi >= 25 && bmi <= 29.9) {
    return 2;
  } else if (bmi >= 30 && bmi <= 34.9) {
    return 3;
  } else if (bmi >= 35 && bmi <= 39.9) {
    return 4;
  }

  return 5;
};

const getInterval = bmiIntervalIndex => {
  return bmiIntervals[bmiIntervalIndex].interval;
};

const getLegend = bmiIntervalIndex => {
  return bmiIntervals[bmiIntervalIndex].legend;
};

const getDescription = bmiIntervalIndex => {
  return bmiIntervals[bmiIntervalIndex].description;
};

module.exports = {
  squaredHeight,
  calcBMI,
  calcIdealBW,
  calcMinBW,
  calcMaxBW,
  getBmiIntervalIndex,
  getInterval,
  getLegend,
  getDescription,
};
