const passport = require('passport');
const keys = require('../config/keys');

module.exports = app => {
  app.get(
    '/login/google',
    passport.authenticate('google', {
      scope: ['profile', 'email'],
    })
  );

  app.get(
    '/login/google/callback',
    passport.authenticate('google', {
      failureRedirect: keys.clientURL,
    }),
    (req, res) => res.redirect(keys.clientURL)
  );

  app.get('/login/facebook', passport.authenticate('facebook'));

  app.get(
    '/login/facebook/callback',
    passport.authenticate('facebook', {
      failureRedirect: keys.clientURL,
    }),
    (req, res) => res.redirect(keys.clientURL)
  );

  app.get('/login/twitter', passport.authenticate('twitter'));

  app.get(
    '/login/twitter/callback',
    passport.authenticate('twitter', {
      failureRedirect: keys.clientURL,
    }),
    (req, res) => res.redirect(keys.clientURL)
  );
};
