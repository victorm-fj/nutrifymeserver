module.exports = {
  extends: 'airbnb',
  plugins: ['react', 'jsx-a11y', 'import'],
  rules: {
    'no-console': 0,
    'arrow-body-style': 0,
    'consistent-return': 0,
    'no-shadow': 0,
    'arrow-parens': 0,
    'no-underscore-dangle': 0,
    'no-mixed-operators': 0,
    'no-param-reassign': ['error', { props: false }],
    'comma-dangle': 0,
    'no-useless-escape': 0,
  },
};
